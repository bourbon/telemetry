#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>

#define SD_CACHE_SIZE 512
#define SD_CS_PIN 10

File file;
Adafruit_BNO055 bno = Adafruit_BNO055(55);

int sd_cache_len = 0;


int writeToFile(File file, char* data) {
    if (sd_cache_len + strlen(data) > SD_CACHE_SIZE) {
        Serial.println("Flushing SD cache");
        file.flush();
        sd_cache_len = 0;
    }
    int count = file.println(data);
    Serial.print("Wrote ");
    Serial.println(count);

    sd_cache_len += count;
    return count;
}

char* getEventSensorStr(char* str, sensors_vec_t sensor) {
    char x[10];
    char y[10];
    char z[10];

    dtostrf(sensor.x, 3, 4, x);
    dtostrf(sensor.y, 3, 4, y);
    dtostrf(sensor.z, 3, 4, z);

    snprintf(str, 64, "%s,%s,%s", x, y, z);
    return str;
}

char* eventToCsv(sensors_event_t event, char* csv) {
    char orientation[64];
    getEventSensorStr(orientation, event.orientation);
    
    snprintf(csv, 128, "%lu,%s", millis(), orientation);
    Serial.println(csv);
    // TODO - do we need orientation, magnetic, accel, and gyro?
    return csv;
}

void fail() {
    SPI.end();  // SPI might be holding onto pin 13

    pinMode(13, OUTPUT);
    while (1) {
        digitalWrite(13, LOW);
        delay(1000);
        digitalWrite(13, HIGH);
        delay(1000);
    }
}

void initSD() {
    pinMode(SD_CS_PIN, OUTPUT);
    if (! SD.begin(SD_CS_PIN)) {
        Serial.println("SD module not found!");
        fail();
    }

    int ctr = 0;
    char filename[64];
    do {
        snprintf(filename, 64, "sensor%02d.csv", ctr++);
    } while (SD.exists(filename));
    Serial.print("Writing sensor data to: ");
    Serial.println(filename);

    file = SD.open(filename, FILE_WRITE);
}

void initSensor() {
    if (! bno.begin()) {
        Serial.println("Sensor module not found!");
        fail();
    }
    
    bno.setExtCrystalUse(true);
}

void setup() {
    Serial.begin(115200);
    Wire.begin();
    
    // setup SD board
    initSD();

    // setup BNO055 9-DOF Sensor
    initSensor();
}

void loop() {
    // Get sensor event
    sensors_event_t event;
    bno.getEvent(&event);

    // Convert sensor data to string
    char csv[128];
    eventToCsv(event, csv);

    // Write string to disk
    writeToFile(file, csv);

    // TODO - wait for next tick?  BNO055 is 100Hz resolution
    delay(500);  // delay for testing
}
